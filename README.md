# Water system calibration plugin #

A plugin for [Pyforms Generic Editor Welcome plugin](https://bitbucket.org/fchampalimaud/pybpod-gui-plugin).
It is useful to calibrate the valve given the quntity of water needed.

It is tested on Ubuntu with Python 3.

## How to install:

1. On your Desktop, create a new folder on your favorite location (e.g. "plugins")
2. Move this plugin folder to that location
3. Open GUI
4. Select Options > Edit user settings
5. Add the following info:

```python
    GENERIC_EDITOR_PLUGINS_PATH = 'PATH TO PLUGINS FOLDER' # USE DOUBLE SLASH FOR PATHS ON WINDOWS
    GENERIC_EDITOR_PLUGINS_LIST = [
        (...other plugins...),
        'water_calibration_plugin',
    ]
```
6. Save
7. Restart GUI
8. Select Tools and open the plugin

## How to use
The plugin opens and closes N times the valves of a choosen Bpod till the calibration.
The water is collected in a cup and when the protocol finished, a popup windows 
is shown to fill the weigh (gr) of water obtained. The protocol then compares it 
to the target value, if it's the same then the  calibration is complete, 
if not then it reads the last history event and adjusts the time of opening valves 
to calibrate them.  

![waterCalibrationPlugin home](images/windowHome.png)

### Variables:
   - \# Pulses: how many times the valves are opened and closed.
   - Interval: Time interval between pulses in seconds.
   - Tolerance: the tolerace of the result comparing with the Quantity needs.
   - Board: Bpod to use. You can choose one of the Bpods saved into Bpod Boxes of the Project.
   - Ports: wich ports calibrate
   - Valve\# (micro liters): qunatity of water to obtain at the end from the valve \#
   - Time Valve\# (seconds): timer of valve \# in seconds (the last values of calibration are loaded)
   - Time result Valve\# (seconds): timer resulted of valve \# in seconds to use into your protocol

### Buttons:
   - Run: run the calibration.
   - Stop: stop the calibration (to be implemented).
   - Info: info about the plugin. 



![waterCalibrationPlugin home](images/history.png)

### Buttons:
   - Delete: remove the row of data given the index.
   
   
   
## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`


  



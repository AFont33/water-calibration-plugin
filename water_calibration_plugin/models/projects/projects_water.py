# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pyforms import conf
from AnyQt.QtGui import QIcon
from water_calibration_plugin.water_windows import WaterWindow


class ProjectsWater(object):
    def __init__(self, mainwindow=None):
        super(ProjectsWater, self).__init__(mainwindow)

        self.isOpen = False # Useful to know if the windows was opened or not
    # Open it when the Gui is opened
    # self.open_water_plugin()

    def register_on_main_menu(self, mainmenu):
        super(ProjectsWater, self).register_on_main_menu(mainmenu)

        if len([m for m in mainmenu if 'Tools' in m.keys()]) == 0:
             mainmenu.append({'Tools': []})

        menu_index = 0
        for i, m in enumerate(mainmenu):
            if 'Tools' in m.keys():
                menu_index = i
                break


        mainmenu[menu_index]['Tools'].append('-')
        mainmenu[menu_index]['Tools'].append(
            {'Water Calibration': self.open_water_plugin, 'icon': QIcon(conf.WATER_PLUGIN_ICON)})

    def open_water_plugin(self):
        if not hasattr(self, 'water_plugin'):
            self.water_plugin = WaterWindow(self)
            check = self.water_plugin.show()
            if check:
                self.water_plugin.subwindow.resize(*conf.WATER_PLUGIN_WINDOW_SIZE)
                self.isOpen = True
        else:
            check = self.water_plugin.show()
            if not self.isOpen and check:
                self.water_plugin.subwindow.resize(*conf.WATER_PLUGIN_WINDOW_SIZE)
                self.isOpen = True

        return self.water_plugin

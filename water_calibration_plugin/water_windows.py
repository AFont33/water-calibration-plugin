# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging
from AnyQt.QtCore import QTimer, QEventLoop

from pyforms import BaseWidget
from pyforms.controls import ControlText
from pyforms.controls import ControlButton
from pyforms.controls import ControlCombo
from pyforms.controls import ControlCheckBox
from pyforms.controls import ControlProgress
from pyforms.controls import ControlLabel
from pyforms.controls import ControlList
import water_calibration_plugin.settings as settings
import water_calibration_plugin.data_utils as dut
from water_calibration_plugin.default_values import DefaultValues
from water_calibration_plugin.control_gui import ControlGui
from pybpodgui_api.models.board import Board

logger = logging.getLogger(__name__)


class WaterWindow(BaseWidget):
    """
    See:
    - pyforms_generic_editor.models.projects.__init__.py
    - pyforms_generic_editor.models.projects.projects_window.py
    """
    def __init__(self, projects):
        BaseWidget.__init__(self, 'Water Calibration', parent_win=projects.mainwindow)
        self.projects = projects

        self.set_margin(10)
        # self.setMaximumSize(800, 600)

        self.WATER_IS_RUNNING = False # flag to check if ther is a protocol running

        # Load the last values
        self.defaultVariables = DefaultValues(self)
        self.values = self.defaultVariables.getValues()

        # Class to control the GUI
        self.controlGui = ControlGui(self)

        ## Definition of the forms fields
        # Ports
        self._microLneedsP1 = ControlText('Valve1 (microL):', default=self.values['_microLneeds'][0], helptext ='Quantity of water delivered from Port 1.')
        self._microLneedsP2 = ControlText('Valve2 (microL):', default=self.values['_microLneeds'][1], helptext ='Quantity of water delivered from Port 2.')
        self._microLneedsP3 = ControlText('Valve3 (microL):', default=self.values['_microLneeds'][2], helptext ='Quantity of water delivered from Port 3.')
        self._microLneedsP4 = ControlText('Valve4 (microL):', default=self.values['_microLneeds'][3], helptext ='Quantity of water delivered from Port 4.')
        self._microLneedsP5 = ControlText('Valve5 (microL):', default=self.values['_microLneeds'][4], helptext ='Quantity of water delivered from Port 5.')
        self._microLneedsP6 = ControlText('Valve6 (microL):', default=self.values['_microLneeds'][5], helptext ='Quantity of water delivered from Port 6.')
        self._microLneedsP7 = ControlText('Valve7 (microL):', default=self.values['_microLneeds'][6], helptext ='Quantity of water delivered from Port 7.')
        self._microLneedsP8 = ControlText('Valve8 (microL):', default=self.values['_microLneeds'][7], helptext ='Quantity of water delivered from Port 8.')

        self._firstTimeP1 = ControlText('Time Valve1 (s)', default=self.values['_firstTimes'][0], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP2 = ControlText('Time Valve2 (s)', default=self.values['_firstTimes'][1], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP3 = ControlText('Time Valve3 (s)', default=self.values['_firstTimes'][2], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP4 = ControlText('Time Valve4 (s)', default=self.values['_firstTimes'][3], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP5 = ControlText('Time Valve5 (s)', default=self.values['_firstTimes'][4], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP6 = ControlText('Time Valve6 (s)', default=self.values['_firstTimes'][5], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP7 = ControlText('Time Valve7 (s)', default=self.values['_firstTimes'][6], helptext ='timer of valve in seconds (the last values of calibration are loaded).')
        self._firstTimeP8 = ControlText('Time Valve8 (s)', default=self.values['_firstTimes'][7], helptext ='timer of valve in seconds (the last values of calibration are loaded).')

        self._resultP1 = ControlText('Time result Valve1 (s):', helptext='Time resulted to use into the protocols for Valve 1.')
        self._resultP2 = ControlText('Time result Valve2 (s):', helptext='Time resulted to use into the protocols for Valve 2.')
        self._resultP3 = ControlText('Time result Valve3 (s):', helptext='Time resulted to use into the protocols for Valve 3.')
        self._resultP4 = ControlText('Time result Valve4 (s):', helptext='Time resulted to use into the protocols for Valve 4.')
        self._resultP5 = ControlText('Time result Valve5 (s):', helptext='Time resulted to use into the protocols for Valve 5.')
        self._resultP6 = ControlText('Time result Valve6 (s):', helptext='Time resulted to use into the protocols for Valve 6.')
        self._resultP7 = ControlText('Time result Valve7 (s):', helptext='Time resulted to use into the protocols for Valve 7.')
        self._resultP8 = ControlText('Time result Valve8 (s):', helptext='Time resulted to use into the protocols for Valve 8.')

        self._port1 = ControlCheckBox('Port1')
        self._port2 = ControlCheckBox('Port2')
        self._port3 = ControlCheckBox('Port3')
        self._port4 = ControlCheckBox('Port4')
        self._port5 = ControlCheckBox('Port5')
        self._port6 = ControlCheckBox('Port6')
        self._port7 = ControlCheckBox('Port7')
        self._port8 = ControlCheckBox('Port8')

        # List of ports and variables
        self._allports       = [self._port1, self._port2, self._port3, self._port4, self._port5, self._port6, self._port7, self._port8]
        self._allmicroLneeds = [self._microLneedsP1, self._microLneedsP2, self._microLneedsP3, self._microLneedsP4, self._microLneedsP5,
                                self._microLneedsP6, self._microLneedsP7, self._microLneedsP8]
        self._allresults     = [self._resultP1, self._resultP2, self._resultP3, self._resultP4, self._resultP5, self._resultP6,
                                self._resultP7, self._resultP8]
        self._allfirstTimes  = [self._firstTimeP1, self._firstTimeP2, self._firstTimeP3, self._firstTimeP4, self._firstTimeP5,
                                self._firstTimeP6, self._firstTimeP7, self._firstTimeP8]

        # General controls
        self._interval  = ControlText('Interval: ', default=self.values['_interval'], helptext='Time interval between pulses (seconds).')
        self._cyles     = ControlText('# pulses: ', default=self.values['_cyles'], helptext='How many times the valves are opened.')
        self._tolerance = ControlText('Tolerance (±microL): ', default=self.values['_tolerance'], helptext='Tolerance of the quantity resulted.')
        self._board     = ControlCombo('Board')

        # Other controls
        self._playbutton    = ControlButton('Run')
        self._stopbutton    = ControlButton('Stop')
        self.__switchButton()
        self._infobutton    = ControlButton('Info')
        self._refreshbutton = ControlButton('Refresh Plugin')
        self._progress      = ControlProgress('Running...')

        self._title         = ControlLabel('Water delivery.')
        self._legendHistory = ControlLabel('*** not calibrated: stopped by the user.')
        self._removeHistory = ControlButton('Delete..')


        # History
        self._listW                    = ControlList('History data')
        self._listW.readonly           = True
        self._listW.horizontal_headers = ['#', 'Date', 'Cycle', 'Tolerance', 'Board',
                                          'microL V1', 'Time V1', 'grResult 1',
                                          'microL V2', 'Time V2', 'grResult 2',
                                          'microL V3', 'Time V3', 'grResult 3',
                                          'microL V4', 'Time V4', 'grResult 4',
                                          'microL V5', 'Time V5', 'grResult 5',
                                          'microL V6', 'Time V6', 'grResult 6',
                                          'microL V7', 'Time V7', 'grResult 7',
                                          'microL V8', 'Time V8', 'grResult 8']
        self._listW.set_sorting_enabled(True)

        # Define the organization of the forms
        self.formset = [('_title', ' ', ' ', '_refreshbutton','_infobutton'),
                        {'Calibration': [
                            ('_cyles', '_interval', '_tolerance', '_board', ' ', ' '),
                            (' ', '_playbutton', '_stopbutton', ' '),
                            ('_progress'),
                            ('_port1', '_microLneedsP1', '_firstTimeP1', ' ', ' ', '_resultP1'),
                            ('_port2', '_microLneedsP2', '_firstTimeP2', ' ', ' ', '_resultP2'),
                            ('_port3', '_microLneedsP3', '_firstTimeP3', ' ', ' ', '_resultP3'),
                            ('_port4', '_microLneedsP4', '_firstTimeP4', ' ', ' ', '_resultP4'),
                            ('_port5', '_microLneedsP5', '_firstTimeP5', ' ', ' ', '_resultP5'),
                            ('_port6', '_microLneedsP6', '_firstTimeP6', ' ', ' ', '_resultP6'),
                            ('_port7', '_microLneedsP7', '_firstTimeP7', ' ', ' ', '_resultP7'),
                            ('_port8', '_microLneedsP8', '_firstTimeP8', ' ', ' ', '_resultP8')
                        ],
                        'History':[('_legendHistory', ('_removeHistory')), ('_listW')],
                        } , ' ']

        # Hide the forms
        for form in self._allresults:
            form.enabled = False
        for form in self._allfirstTimes:
            form.enabled = False
        for form in self._allmicroLneeds:
            form.enabled = False
        self._progress.hide()

        # Loop for the task
        self._timer = QTimer()


        # Load all the data
        self.myData = dut.WaterDataUtils()
        self.showHistoryList()

        #Load ports activeted last time
        for i in range(len(self.values['_ports'])):
            if self.values['_ports'][i]:
                self._allports[i].value = True

        # Set the change events
        for port in self._allports:
            port.changed_event = self.__port_changed_evt

        # Define the button action
        self._playbutton.value = self.__playbuttonAction
        self._stopbutton.value = self.__stopbuttonAction
        self._infobutton.value = self.__infobuttonAction
        self._refreshbutton.value = self.___refreshbuttonAction
        self._removeHistory.value = self.__removebuttonAction
        #self.__updateBoard()
        self._board.changed_event = self.__board_changed_evt

        self.__updateDataForm()

    def __updateBoard(self):
        self.value = self.defaultVariables.getValues()
        self._board.clear()
        i = None
        try:  # read all the board available
            if not self.controlGui.has_boards():
                self.warning('No Board exists. Add the board to the project.', "Unexpected error")
                self._board.add_item('', 0)
                return
            else:
                count = 1
                self._board.add_item('', 0)
                for b in self.controlGui.get_boards(): 
                    self._board.add_item(b.name, b)
                    if b.name == self.values['_board']: 
                        i = count
                    else: count += 1
        except Exception as err:
            pass


        if i is not None:
            self._board.current_index = i
            self.__board_changed_evt()
        else: 
            self._board.current_index = 0
        

    def __updateDataForm(self):
        self.__loadDataB()
        L = len(self.vecDataW[0])
        for i in range(len(self._allports)):
            if self._allports[i].value:
                if L > 0 and not self.WATER_IS_RUNNING:
                    # Control for bpod
                    if self.vecDataW[2][i][L-1] > settings.MAX_VAL:
                        self._allfirstTimes[i].value = str(settings.MAX_VAL)
                        self.warning('The last calibration gives a time bigger then MAX_VAL so the '
                                    'TimeValve ' + str(i+1) + ' is setted to ' + str(settings.MAX_VAL) + '.',
                                    "Value error")
                    elif self.vecDataW[2][i][L-1] < settings.MIN_VAL:
                        self._allfirstTimes[i].value = str(settings.MIN_VAL)
                        self.warning('The last calibration gives a time lower then MIN_VAL so the '
                                    'TimeValve '  + str(i+1) + ' is setted to ' + str(settings.MIN_VAL) + '.',
                                     "Value error")
                    else:
                        self._allfirstTimes[i].value = str(self.vecDataW[2][i][L-1])

                    self._allmicroLneeds[i].value = str(self.vecDataW[1][i][L-1])
            else:
                self._allfirstTimes[i].value  = '0'
                self._allmicroLneeds[i].value = '0'

    def __playbuttonAction(self):
        """Button action event"""
        self.__port_changed_evt()
        self.controlGui.check() # Check the structure

        if self.WATER_IS_RUNNING:
            self.warning('The calibration is running. Wait the end.', "Unexpected error")
            return
        else:
            self.WATER_IS_RUNNING = True
            self.__switchButton()
        try:
            self.__updateDataForm() # Update the forms
            

            # Values conversion
            self.microLNeeds = [float(mLneeds.value) for mLneeds in self._allmicroLneeds]
            self.firstTimes  = [float(t.value) for t in self._allfirstTimes]
            self.interval = float(self._interval.value)
            self.tolerance = float(self._tolerance.value)
            self.cycles = int(self._cyles.value)
   
            # Set the parameters depending on how many ports you are using
            list_ports_for_bpod = ''
            list_portTime_for_bpod = ''
            for i in range(len(self._allports)):
                if self._allports[i].value:
                    list_ports_for_bpod += str(i+1) + '-'
                    list_portTime_for_bpod += self._allfirstTimes[i].value + '-'
      
            self.controlGui.modify_variable(2, list_ports_for_bpod[:-1]) # removing the last symbol -
            self.controlGui.modify_variable(3, list_portTime_for_bpod[:-1])
       
            # Save all the variables
            self.defaultVariables.save()

            # Set the variables for the protocol
            self.controlGui.modify_variable(0, self.cycles)       # Loop
            self.controlGui.modify_variable(1, self.interval)  # interval

            # Initialization
            self.portCalibrated = [0 for i in range(settings.TOT_PORTS)]
            self.microLgoal = self.microLNeeds
            self.lastTimes = self.firstTimes[:]
            self.grObtained = [0 for i in range(len(self.firstTimes))]

            # Loop until the tolerance is reached
            self.portsToRecalibrated = ''
            self._stop = False  # flag used to close the loop
            self._wait = False  # flag to switch run task and question
            self.runTask(True) # Run the protocol
            self._timer.timeout.connect(self.runTask)
            if not self._stop:
                self._timer.start(2000) # Start the loop

        except ValueError as err:
            self.warning('Values incorrect. Use numers and the point for the decimals.\n Error:{0}'.format(str(err)),
                         "Unexpected error")
            self.WATER_IS_RUNNING = False
            self.__switchButton()
            return
        except Exception as err:
            self.warning('Error:{0}'.format(str(err)), "Unexpected error")
            self.WATER_IS_RUNNING = False
            self.__switchButton()

    def runTask(self, startP=False):
        """ Start the Task. """
        if startP:
            self._progress.show()
            self._progress.value = 1
            self.nPorts = sum([p.value for p in self._allports])
            self._progress.max = self.nPorts + 1


        try:
            if self.controlGui.boardStatus() == Board.STATUS_READY and not self._stop:
                # if the bpod is free and the task was not stopped
                if self._wait: # if the bpod finished and it is waiting
                    self.portsToRecalibrated = ''
                    self.timersToUse = ''

                    for p in range(len(self._allports)):
                        self.firstTimes = self.lastTimes[:]
                        # Get the quantity of water obtained
                        if self.portCalibrated[p] == 0 and self._allports[p].value:
                            self.grObtained[p] = self.input_double('Insert the value obtained at Port ' + str(p+1) + '[gr]:',
                                                                       "Cycle Done", min=0.0001, max=100, decimals=4)
                            if self.grObtained[p] is None:
                                self._stop = True
                                self.warning("Calibration stoped: The data will be not saved.", "Warning")
                                break # Exit

                            microLobtained = self.grObtained[p] * settings.GR_TO_MICROL/self.cycles
                            if abs(microLobtained - self.microLgoal[p]) < self.tolerance: # Check of calibration
                                # If the port is calibrated
                                self.portCalibrated[p] = 1
                                self._progress += 1  # update the progression bar
                                self._allresults[p].value = str(round(self.lastTimes[p], 3))
                            else:
                                self.lastTimes[p]         = (self.lastTimes[p] * self.microLgoal[p]) / microLobtained  # Proportion computation
                                if self.lastTimes[p] > settings.MAX_VAL or self.lastTimes[p] < settings.MIN_VAL:
                                    self._stop = True
                                    self.warning("Calibration stoped: the value for calibtation saturated. \n The "
                                        "value resulted is:" + str(self.lastTimes[p]) + " at Port" + str(p+1), "Warning")
                                    break # Exit
                                self.timersToUse         += str(self.lastTimes[p]) + '-'
                                self.portsToRecalibrated += str(p+1) +  '-'

                    if not self._stop:
                        if self.nPorts == sum(self.portCalibrated): # Stop if all ports are calibrated
                            # Save the data and stop the task
                            self.myData.saveWaterResult(self.lastTimes, self.microLNeeds, self.grObtained,
                                                        self.cycles, self.tolerance, self._board.text)
                            self._stop = True
                            self.showHistoryList()  # Upload the list
                            self.info("Calibration completed.", "Succesful")
                        else:
                            ok = self.question("Do you want to continue?", "Not calibrated yet")
                            if ok == 'yes':
                                self.controlGui.modify_variable(2, self.portsToRecalibrated[:-1])  # Recalibrate the ports still not calibrated
                                self.controlGui.modify_variable(3, self.timersToUse[:-1])  # Valve timer
                                self._wait = False # Flag to run the bpod and check the results
                            else:
                                self._stop = True
                                # Save the data and stop the task
                                self.myData.saveWaterResult(self.firstTimes, self.microLNeeds, self.grObtained,
                                                            self.cycles, self.tolerance, self._board.text, True)
                                self.showHistoryList()  # Upload the list
                                self.info("Calibration stoped by the user: the data will be saved.", "Info")

                else:
                    # Run the protocol_water
                    self.controlGui.run_task()
                    self._wait = True # Flag to run the bpod and check the results

            if self._stop: # Stop the task
                self._progress.hide()
                self._timer.stop()
                self.WATER_IS_RUNNING = False
                self.__switchButton()


            QEventLoop()

        except Exception as err:
            self._timer.stop()
            logger.error(str(err), exc_info=True)
            self.critical("Unexpected error while running water calibration. Pleas see log for more details.",
                          "Error")
            self.WATER_IS_RUNNING = False
            self.__switchButton()

    def __stopbuttonAction(self):
        """Button action event"""
        try:
            self._stop = True
            self.controlGui.stopBpod()
            self.info('The calibration is stopped.', "Stop")
        except Exception as err:
            self.warning('No calibration is running. \n Error:{0}'.format(str(err)), "Unexpected error")

    def __infobuttonAction(self):
        """Button action event"""
        self.info('The plugin opens and closes N times the valves (one each time) of a choosen Bpod till '
                'the calibrion. \n The water is collected in a cup and when the protocol finished, a popup '
                'windows  is shown to fill the weigh (gr) of water obtained. The protocol then compares '
                'it to the target value, if it is the same then the calibration is completed, if not then '
                'it reads the last history events and adjust the time of the open valves calibrating again.',
                "Info water calibration")

    def ___refreshbuttonAction(self):
        # Create a new experiment WaterProtocol if it does not exist
        self.controlGui.check()
        # List of Bpods
        self.__updateBoard()

    def __removebuttonAction(self):
        idx = self.input_int("Write the index # that you want to delete.","Index", min=0)
        if idx is not None:
            checkErr = self.myData.remove(idx)
            if checkErr is None:
                self.showHistoryList()
            else:
                self.warning('Incorrect index. Please try again. \n Error:{0}'.format(str(checkErr)),
                             "Unexpected error")

    def __board_changed_evt(self):
        """
        React to changes on board value
        """
        self.controlGui.modify_board(self._board.value)
        if self._board.current_index == 0:
            return
        self.defaultVariables.save()
        self.__port_changed_evt()
        
    def __port_changed_evt(self):
        """
        React to changes of the port flags
        """
        try:
            for i in range(len(self._allports)):
                if self._allports[i].value:
                    self._allfirstTimes[i].enabled  = True
                    self._allmicroLneeds[i].enabled = True
                    if not self.controlGui.is_port_open(i):
                        self.controlGui.enable_port(i)
                        self.warning('Behavior Port' + str(i+1) + ' is disabled. The water plugin enabled it.', 'Warning')

                else:
                    self._allfirstTimes[i].enabled  = False
                    self._allmicroLneeds[i].enabled = False
                    if self.controlGui.is_port_open(i):
                        self.controlGui.disable_port(i)
                        self.warning('Behavior Port' + str(i+1) + ' is enabled. The water plugin disabled it.', 'Warning')

        except Exception:
            if self._board.current_index == 0:
                return
            self.warning('Load first the ports on Bpod boards: ' + str(self._board.text), 'Warning')
            return

    def showHistoryList(self):
        """ Show history """
        self.__loadDataB()

        self._listW.clear()
        L = len(self.vecDataW[0])
        self._counter = 0

        for i in range(L):
            row = [self._counter]
            if self.vecDataW[7][i]:
                row += [str(self.vecDataW[0][i])+'***'] # Date first
            else:
                row += [str(self.vecDataW[0][i])]  # Date first
            row += [str(self.vecDataW[k][i]) for k in range(4, 7)]  # other data
            for p in range(settings.TOT_PORTS):
                row += ['-' if self.vecDataW[1][p][i] == 0 else str(self.vecDataW[1][p][i])] # microLneeds
                row += ['-' if self.vecDataW[2][p][i] == 0 else str(self.vecDataW[2][p][i])] # results
                row += ['-' if self.vecDataW[3][p][i] == 0 else str(self.vecDataW[3][p][i])] # gr
            row += []
            self._listW += row
            self._counter += 1

    def __loadDataB(self):
        """ Load data. """
        self.vecDataW = self.myData.getWaterResult()

    def show(self):
        """ Prevent the call to be recursive because of the mdi_area """
        if len(self.projects.projects) == 0:
            reply = self.warning('Open a project or create a new one.','No project exists')
            return False
        # CONTROLS BEFORE OPEN THE PLUGIN
        elif self.projects.projects[0].path is None or len(self.projects.projects[0].path) == 0 or not \
        self.projects.projects[0].is_saved():
            reply = self.warning('Err001: To import a protocol you need to save the project first.','Project not saved yet')
            return False
        elif len(self.projects.projects[0].boards) == 0:
            self.warning('Err002: No Board exists. Add the board to the project.', "Unexpected error")
            return False
        else:
            if hasattr(self, '_show_called'):
                BaseWidget.show(self)
                return
            self._show_called = True
            self.mainwindow.mdi_area += self
            del self._show_called

            self._locals = locals()
            self._globals = globals()

            # Create a new experiment WaterProtocol if it does not exist
            self.controlGui.check()
            # List of Bpods
            self.__updateBoard()
            return True

    def __switchButton(self):
        """ Swith the buttons during the calibration"""
        if self.WATER_IS_RUNNING:
            self._playbutton.enabled = False
            self._stopbutton.enabled = True
        else:
            self._playbutton.enabled = True
            self._stopbutton.enabled = False

    def beforeClose(self):
        return False

    @property
    def mainwindow(self):
        return self.projects.mainwindow


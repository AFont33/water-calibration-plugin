# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" Class useful to contain all the method that interacs with the GUI"""
import water_calibration_plugin.settings as settings

class ControlGui:

    def __init__(self, gui):
        #self.gui = gui
        self._gui          = gui
        self._project      = None
        self._boards       = None
        self._boardIdx     = None
        self._experiment   = None
        self._setup        = None
        self._task         = None
        self._variables    = None
        self.check()

    def check(self):
        """ Check all the parameters """
        self.__check_project()
        self.__check_board()
        self.__check_task()
        self.__check_experiment()
        self.__check_setup()
        self.__check_variables()

        # Assign the protocol
        self._setup.task = self._task

    def stopBpod(self):
        self._setup.stop_task()

    def boardStatus(self):
        return self._boards[self._boardIdx].status

    def enable_port(self, port):
        list_ports = self._boards[self._boardIdx].enabled_behaviorports
        list_ports[port] = True
        self._boards[self._boardIdx].enabled_behaviorports = list_ports

    def disable_port(self, port):
        list_ports = self._boards[self._boardIdx].enabled_behaviorports
        list_ports[port] = False
        self._boards[self._boardIdx].enabled_behaviorports = list_ports

    def has_boards(self):
        """ Check if there is at least one board"""
        return len(self._boards) > 0 

    def get_boards(self):
        return self._boards

    def modify_variable(self, idx, value):
        """ Modify a value of a variable """
        self._variables[idx].value = str(value)
        self.__saveProject()

    def modify_board(self, value):
        """ Modify the board of the setup """
        self.__check_board()
        self._setup.board = value
        self.__saveProject()

    def run_task(self):
        """ Run Bpod """
        self._setup.run_task()

    def __check_task(self):
        """ Check if the tast exists"""
        found = False
        for t in self._project.tasks: # Try to find it
            if t.name == settings.WATER_PLUGIN_NAME_TASK:
                self._task = t
                found = True
                break

        if self._task is None or not found: # Create it
            self._task = self._project.import_task(settings.WATER_PLUGIN_PATH_TASK)
            self._task.hide()

    def __check_experiment(self):
        """ Check if the experiment exists"""
        found = False
        for e in self._project.experiments: # Try to find it
            if e.name == settings.WATER_PLUGIN_NAME_EXPERIMENT:
                self._experiment = e
                found = True
                break

        if self._experiment is None or not found: # Create the experiment and its setup
            # Create the experiment
            self._experiment = self._project.create_experiment()
            self._experiment.hide()
            self._experiment.name = settings.WATER_PLUGIN_NAME_EXPERIMENT
            
            # Create the setup
            self._setup = self._experiment.create_setup()
            self._setup.name = settings.WATER_PLUGIN_NAME_SETUPS
            self.__createVariables()

    def __check_setup(self):
        """ Check if the setup exists"""
        found = False
        for s in self._experiment.setups: # Try to find it
            if s.name == settings.WATER_PLUGIN_NAME_SETUPS:
                self._setup = s
                found = True
                break

        if self._setup is None or not found: # Create it
            # Create the setup
            self._setup = self._experiment.create_setup()
            self._setup.name = settings.WATER_PLUGIN_NAME_SETUPS
            self.__createVariables()
        # Check variables
        if len(self._setup.board_task.variables) == 0:
            self.__createVariables()

    def __check_variables(self):
        if self._variables is None:
            self._variables = []
            for v in self._setup.board_task.variables:
                self._variables.append(v)

    def __createVariables(self):
        self._variables = []
        # Var_0: index of loop
        self._variables.append(self._setup.board_task.create_variable('VAR_0', '5'))
        # Var_1: interval
        self._variables.append(self._setup.board_task.create_variable('VAR_1', '0.1'))
        # Var_2: ports used
        self._variables.append(self._setup.board_task.create_variable('VAR_2', 'None'))
        # Var_3: timer valves
        self._variables.append(self._setup.board_task.create_variable('VAR_3', '0.1'))

    def __check_board(self):
        self._boards  = self._project.boards

    def __check_project(self):
        self._project = self._gui.projects.projects[0]

    def __saveProject(self):
        self._project.save()

    def is_port_open(self, port):
        self.__find_boardIdx()
        return self._boards[self._boardIdx].enabled_behaviorports[port]

    def __find_boardIdx(self):
        for i in range(len(self._boards)):
            if self._boards[i] == self._setup.board:
                self._boardIdx = i
                break

